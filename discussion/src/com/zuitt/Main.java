//directory that contains files that is used to build an application
//reverse domain name notation
package com.zuitt;
import java.util.Scanner;


//Main class is the entry point  of a java program and is responsible for executing our code.
public class Main {

    public static int computeAll(int x, int y){
        return x+y;
    }

    public static void main(String[] args) {

        Student stud = new Student();
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter family name: ");
        String famname = sc.next();
        stud.setFamilyname(famname);

        System.out.println(stud.getFamilyname());
    }
}