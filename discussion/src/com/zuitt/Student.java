package com.zuitt;

public class Student extends Person {
    private String familyname;
    private int age;
    private int studID;

    //constructor
    public Student(){}
    public Student(String familyname, int age, int studID){
        this.familyname=familyname;
        this.age=age;
        this.studID=studID;
    }

    //setter
    public void setFamilyname(String famname){
        this.familyname=famname;
    }

    public void setAge(int age){
        this.age = age;
    }

    public void setStudID(int studID){
        this.studID = studID;
    }

    //getter

    public String getFamilyname(){
        return familyname;
    }

    public int getAge(){return age;}

    public int getStudID(){return studID;}


    public String toString(){
        return "Family Name: " + getFamilyname() + ",\t" + "Age: " + getAge() + ", \t" + "ID: " + getStudID();
    }

}
